import { Box, Button, Container, TextField, Typography } from '@material-ui/core'
import React, { Component } from 'react'
import { signIn } from '../../redux/actions/auth';
import {connect} from "react-redux";
import Header from '../../components/Header';

class Signin extends Component {
    constructor(props){
        super(props);
        this.state = {
            credentials: {
                taiKhoan: "",
                matKhau: "",
            },
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target;
        this.setState({
            credentials: {...this.state.credentials, [name]: value},
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.credentials);
        this.props.dispatch(signIn(this.state.credentials, this.props.history));
        // this.props.history.push("/");
    }

    render() {
        console.log(this.props.accessToken);
        return (
            <div>
                <Header/>
                <Container maxWidth="sm">
                    <Typography component="h1" variant="h2">Sign In</Typography>
                    <form onSubmit={this.handleSubmit}>
                        <Box my={3}>
                            <TextField name="taiKhoan" onChange={this.handleChange} variant="outlined" label="Username" fullWidth></TextField>
                        </Box>
                        <Box my={3}>
                            <TextField name="matKhau" onChange={this.handleChange} variant="outlined" label="Password" fullWidth type="password"></TextField>
                        </Box>
                        <Box textAlign="center">
                            <Button type="submit" variant="contained" color="primary">Submit</Button>
                        </Box>
                    </form>

                </Container>
            </div>
        )
    }
}

const mapDispatchToProps = (state) => {
    return {
        accessToken: state.credentials.token
    };
}

export default connect(mapDispatchToProps)(Signin);