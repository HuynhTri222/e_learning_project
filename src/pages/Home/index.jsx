import React, { Component } from "react";
import { Typography, Container, Grid, withStyles } from "@material-ui/core";
import CourseItem from "../../components/CourseItem";
import { connect } from "react-redux";
// import  axios  from "axios";
import { style } from "./style";
// import { SET_COURSES } from "../../redux/actions/type";
// import { actionCreator } from "../../redux/actions";
import { fetchCourses } from "../../redux/actions/course";
import Header from "../../components/Header";


class Home extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Typography
          className={this.props.classes.title}
          component="h1"
          variant="h1">
          Danh sach khoa hoc
        </Typography>
        <Container maxWidth="lg">
          <Grid container spacing={3}>
            {this.props.courseList.map((item, index) => {
              return(<Grid key={index} item xs={12} sm={6} md={3} lg={3} xl={3}>
                <CourseItem item={item}/>
            </Grid>);
            })}
          </Grid>
        </Container>
      </div>
    );
  }
  componentDidMount() {
    this.props.dispatch(fetchCourses);
  }
}

const mapStateToProps = (state) => {
  return {
    courseList: state.course.courseList,
  }
}

export default connect(mapStateToProps)(withStyles(style, {withTheme: true})(Home));
