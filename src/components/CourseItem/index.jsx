import React, { Component } from 'react'
import { Card, CardMedia, CardContent, Typography, CardActions, CardActionArea, Button, withStyles } from '@material-ui/core';
import { style } from "./style";
//giup component sai dc router
import { withRouter } from "react-router-dom";

class CourseItem extends Component {

  gotoDetail = () => {
    this.props.history.push("/detail/" +this.props.item.maKhoaHoc);//khong duoc vi khong co history
  }
    render() {
      const {hinhAnh, tenKhoaHoc, moTa, } = this.props.item;
        return (
            <Card>
            <CardActionArea>
              <CardMedia
                className={this.props.classes.img}
                image={hinhAnh}
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  {tenKhoaHoc}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {moTa.substr(0 ,10) + "..."}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button onClick={this.gotoDetail} className={this.props.classes.btn} color="secondary">
                View Detail
              </Button>
            </CardActions>
          </Card>        
          );
    }
}

export default withRouter( withStyles(style, {withTheme: true})(CourseItem) );