export const style = (theme) => {
    return ({
        img: {
            height: 300,
        },
        btn: {
            backgroundColor: theme.palette.background.grey.dark,
            "&:hover": {
                backgroundColor: theme.palette.secondary.light,
            },
            [theme.breakpoints.down("sm")]: {
                color: theme.palette.text.secondary,                
            },
            [theme.breakpoints.up("sm")]: {
                color: theme.palette.text.yellow,                
            }
        }
    });
}