import { AppBar, Button, Toolbar } from '@material-ui/core'
import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {connect} from "react-redux";


class Header extends Component {
    render() {
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Link to="/">
                            <Button color="inherit">Home</Button>
                        </Link>
                        {this.props.isLogin !== "" ?
                            <Button color="inherit">Hello, homie</Button> 
                            : 
                            <>
                            <Link to="/signin">
                               <Button color="inherit">Login</Button>
                            </Link>

                                <Button color="inherit">Sign up</Button>
                            </>
                        }
                                                
                    </Toolbar>
                </AppBar>
            </div>
        )
    }; 

}

const mapDispatchToProps = (state) => {
    return {
        isLogin: state.credentials.token,
    };
}

export default connect(mapDispatchToProps)(Header);