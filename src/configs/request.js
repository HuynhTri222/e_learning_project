import Axios from "axios"

export const request = (url, method, data) => {
    const config = {};
    const token = localStorage.getItem("t");
    if(token){
        config.headers = {
            headers: {//Dinh token vao request, luc nay se luon dc dui den backend moi khi gui request
                Authorization: "Bearer " + token,
            }
        }
    }
   return Axios({
        url,
        method,
        data,
        ...config,
    });
}