import { SET_COURSES } from "../actions/type";

//Quan ly tat ca nhung gi lien quan toi course
let initialState = {
    courseList: [],
}

const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case SET_COURSES:
            return{...state, courseList: payload};    
        default:
            return state;
    }
}

export default reducer;