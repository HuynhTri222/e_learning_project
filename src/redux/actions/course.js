// async action
// import  axios  from "axios";
import { SET_COURSES } from "./type";
import { actionCreator } from "./index";
import { request } from "../../configs/request";


export const fetchCourses = (dispatch) => {
    request(
        "http://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01",
        "GET",
        ).then((res) => {
        console.log(res);
        //dispatch action
        dispatch(actionCreator(SET_COURSES, res.data));
      }).catch((err) => {
        console.log(err);
      });
};
