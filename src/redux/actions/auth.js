// import Axios from "axios";
import { actionCreator } from ".";
import { request } from "../../configs/request";
import { SET_TOKEN } from "./type";

export const signIn = (data, history) => (dispatch) => {
    request(
        "http://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
        "POST",
        data
    ).then((res) => {
        console.log(res);
        localStorage.setItem("t", res.data.accessToken);

        dispatch(actionCreator(SET_TOKEN, res.data.accessToken));
        history.replace("/");

    }).catch((err) => {
        console.log(err);
    });
};

